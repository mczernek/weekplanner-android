package pl.mczernek.weekplanner.action;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import com.hannesdorfmann.mosby.mvp.MvpActivity;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.mczernek.weekplanner.R;
import pl.mczernek.weekplanner.model.action.Action;
import pl.mczernek.weekplanner.model.action.SimpleAction;
import pl.mczernek.weekplanner.week.Day;

public class EditActionActivty extends MvpActivity<ActionEditView, ActionEditPresenter> implements ActionEditView {

    @Bind(R.id.name)
    EditText nameEdit;

    @Bind(R.id.every_week)
    Switch weekSwitch;

    @Bind(R.id.sunday)
    View sunday;
    @Bind(R.id.monday)
    View monday;
    @Bind(R.id.tuesday)
    View tuesday;
    @Bind(R.id.wednesday)
    View wednesday;
    @Bind(R.id.thursday)
    View thursday;
    @Bind(R.id.friday)
    View friday;
    @Bind(R.id.saturday)
    View saturday;

    DayHolder[] dayHolders;


    public static Intent getLaunchIntent(Context ctx) {
        Intent intent = new Intent();
        intent.setClass(ctx, EditActionActivty.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.action_edit_activity);
        ButterKnife.bind(this);
        initDayHolders();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @NonNull
    @Override
    public ActionEditPresenter createPresenter() {
        return new ActionEditPresenterBase();
    }

    @Override
    public void setAction(Action action) {
        nameEdit.setText(action.getDisplayName());
        weekSwitch.setChecked(action.getDays().size() == 7);
    }

    @Override
    public Action createAction() {
        SimpleAction.Builder builder = new SimpleAction.Builder(nameEdit.getText().toString());
        builder.repeakWeekly(weekSwitch.isChecked());
        builder.setDays(getSelectedDays());
        return builder.build();
    }

    @Override
    public void finishEditing() {
        finish();
    }

    public List<Day> getSelectedDays() {
        List<Day> days = new LinkedList<>();
        for (DayHolder d : dayHolders) {
            if (d.view.isSelected()) {
                days.add(d.day);
            }
        }
        return days;
    }

    private void initDayHolders() {
        dayHolders = new DayHolder[7];
        dayHolders[0] = new DayHolder(Day.SUNDAY, sunday);
        dayHolders[1] = new DayHolder(Day.MONDAY, monday);
        dayHolders[2] = new DayHolder(Day.TUESDAY, tuesday);
        dayHolders[3] = new DayHolder(Day.WEDNESDAY, wednesday);
        dayHolders[4] = new DayHolder(Day.THURSDAY, thursday);
        dayHolders[5] = new DayHolder(Day.FRIDAY, friday);
        dayHolders[6] = new DayHolder(Day.SATURDAY, saturday);
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.save)
    public void clickSave() {
        getPresenter().createAction();
    }

    private class DayHolder implements View.OnClickListener {

        private Day day;
        private View view;

        DayHolder(Day day, View view) {
            this.day = day;
            this.view = view;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            view.setSelected(!view.isSelected());
        }
    }

}
