package pl.mczernek.weekplanner.action;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;

public interface ActionEditPresenter extends MvpPresenter<ActionEditView> {

    void createAction();

}
