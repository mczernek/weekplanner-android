package pl.mczernek.weekplanner.action;

import com.hannesdorfmann.mosby.mvp.MvpView;

import pl.mczernek.weekplanner.model.action.Action;

public interface ActionEditView extends MvpView {

    void setAction(Action action);

    Action createAction();

    void finishEditing();

}
