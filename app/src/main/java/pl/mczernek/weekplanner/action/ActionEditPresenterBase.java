package pl.mczernek.weekplanner.action;

import java.lang.ref.WeakReference;

import pl.mczernek.weekplanner.model.action.Action;
import pl.mczernek.weekplanner.model.action.ActionsProvider;

public class ActionEditPresenterBase implements ActionEditPresenter {

    WeakReference<ActionEditView> view;

    @Override
    public void createAction() {
        Action action = getView().createAction();
        ActionsProvider provider = new ActionsProvider();
        provider.addAction(action);
        getView().finishEditing();
    }

    @Override
    public void detachView(boolean retainInstance) {
        view.clear();
    }

    @Override
    public void attachView(ActionEditView view) {
        this.view = new WeakReference<>(view);
    }

    public ActionEditView getView() {
        return view == null ? null : view.get();
    }
}
