package pl.mczernek.weekplanner.week;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

import pl.mczernek.weekplanner.model.action.Action;

/**
 * Created by CRM648 on 1/6/2016.
 */
public interface WeekPlanView extends MvpView, MvpLceView<List<Action>> {

    void showAddNewActionView();

}
