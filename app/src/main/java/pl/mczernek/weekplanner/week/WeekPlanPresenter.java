package pl.mczernek.weekplanner.week;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;

public interface WeekPlanPresenter extends MvpPresenter<WeekPlanView> {

    void loadPlans(boolean pullToRefresh);

    void addNewAction();
}
