package pl.mczernek.weekplanner.week;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.mczernek.weekplanner.R;
import pl.mczernek.weekplanner.action.EditActionActivty;
import pl.mczernek.weekplanner.model.action.Action;

public class WeekPlanActivity extends MvpLceActivity<ViewGroup, List<Action>, WeekPlanView, WeekPlanPresenter> implements WeekPlanView {

    @Bind(R.id.contentView)
    ListView contentView;

    ActionsAdapter adapter = new SimpleActionsAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_week);
        ButterKnife.bind(this);

        contentView.setAdapter(adapter);
        getPresenter().loadPlans(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getResources().getString(R.string.unable_to_load_content);
    }

    @NonNull
    @Override
    public BaseWeekPlanPresenter createPresenter() {
        return new BaseWeekPlanPresenter();
    }

    @Override
    public void setData(List<Action> data) {
        adapter.setData(data);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        getPresenter().loadPlans(pullToRefresh);
    }

    @Override
    public void showAddNewActionView() {
    }

    @OnClick(R.id.fab)
    @SuppressWarnings("unused")
    public void addAction() {
        startActivity(EditActionActivty.getLaunchIntent(this));
    }

    private interface ActionsAdapter extends ListAdapter {
        void setData(List<Action> data);
    }


    private class SimpleActionsAdapter implements ActionsAdapter {

        private ArrayList<Action> data = new ArrayList<>();

        public void setData(List<Action> data) {
            this.data = new ArrayList<>(data);
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Action getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.action_row, parent, false);
            }
            TextView view = (TextView) convertView.findViewById(R.id.text);
            view.setText(getItem(position).getDisplayName());
            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return data.isEmpty();
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }
    }


}
