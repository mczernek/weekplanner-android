package pl.mczernek.weekplanner.week;

import java.lang.ref.WeakReference;
import java.util.List;

import pl.mczernek.weekplanner.model.action.Action;
import pl.mczernek.weekplanner.model.action.ActionsAsyncLoader;
import pl.mczernek.weekplanner.model.action.ActionsLoader;

public class BaseWeekPlanPresenter implements WeekPlanPresenter {

    private WeakReference<WeekPlanView> view;

    @Override
    public void detachView(boolean retainInstance) {
        view.clear();
    }

    @Override
    public void attachView(WeekPlanView view) {
        this.view = new WeakReference<>(view);
    }

    public WeekPlanView getView(){
        return view == null ? null : view.get();
    }

    @Override
    public void addNewAction() {
        getView().showAddNewActionView();
    }

    @Override
    public void loadPlans(final boolean pullToRefresh) {
        getView().showLoading(pullToRefresh);

        ActionsAsyncLoader loader = new ActionsAsyncLoader();
        loader.loadActions(new ActionsLoader.ActionLoaderListener() {
            @Override
            public void onSuccess(List<Action> result) {
                getView().setData(result);
                getView().showContent();
            }

            @Override
            public void onError(Throwable error) {
                getView().showError(error, pullToRefresh);
            }
        });

    }
}
