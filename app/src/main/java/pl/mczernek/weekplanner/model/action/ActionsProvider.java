package pl.mczernek.weekplanner.model.action;

import java.util.LinkedList;
import java.util.List;

public class ActionsProvider {

    private static List<Action> actions = new LinkedList<>();

    public ActionsProvider(){}

    public void addAction(Action ac){
        actions.add(ac);
    }

    public List<Action> getActions(){
        return actions;
    }

}
