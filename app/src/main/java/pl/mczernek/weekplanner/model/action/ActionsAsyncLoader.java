package pl.mczernek.weekplanner.model.action;

import android.os.AsyncTask;

import java.util.LinkedList;
import java.util.List;

public class ActionsAsyncLoader implements ActionsLoader, ActionsLoader.ActionLoaderListener {

    private ActionLoaderListener listener;

    @Override
    public void loadActions(ActionLoaderListener listener) {
        this.listener = listener;
        ActionsAsyncTask task = new ActionsAsyncTask();
        task.execute(null, null);
    }

    @Override
    public void onSuccess(List<Action> result) {
        listener.onSuccess(result);
    }

    @Override
    public void onError(Throwable error) {
        listener.onError(error);
    }

    public class ActionsAsyncTask extends AsyncTask<Void, Void, List<Action>> {

        @Override
        protected List<Action> doInBackground(Void... params) {
            try {
                Thread.sleep(1500);
                ActionsProvider provider = new ActionsProvider();
                return provider.getActions();
            } catch (InterruptedException ex) {
                return null;
            }
        }


        @Override
        protected void onPostExecute(List<Action> result) {
            if (result == null) {
                onError(new Exception("Empty list"));
            } else {
                onSuccess(result);
            }
        }

    }
}
