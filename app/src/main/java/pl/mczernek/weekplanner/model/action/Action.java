package pl.mczernek.weekplanner.model.action;

import java.util.List;

import pl.mczernek.weekplanner.week.Day;

public interface Action {

    String getDisplayName();

    List<Day> getDays();

    boolean repeatWeekly();

}
