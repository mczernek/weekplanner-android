package pl.mczernek.weekplanner.model.action;

import android.text.TextUtils;

import java.util.List;

import pl.mczernek.weekplanner.week.Day;

public class SimpleAction implements Action {

    private String displayName;
    private boolean everyWeek;
    private List<Day> days;

    public SimpleAction(String name, List<Day> days, boolean everyWeek) {
        if (TextUtils.isEmpty(name)) {
            displayName = "";
        } else {
            displayName = name;
        }
        this.days = days;
        this.everyWeek = everyWeek;
    }

    @Override
    public List<Day> getDays() {
        return days;
    }

    @Override
    public boolean repeatWeekly() {
        return everyWeek;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    public static class Builder {

        private String name;
        private boolean everyWeek = false;
        private List<Day> days;

        public Builder(String name) {
            this.name = name;
        }

        public Builder setDays(List<Day> days){
            this.days = days;
            return this;
        }

        public Builder repeakWeekly(boolean everyWeek) {
            this.everyWeek = everyWeek;
            return this;
        }

        public Action build() {
            return new SimpleAction(name, days, everyWeek);
        }


    }
}
