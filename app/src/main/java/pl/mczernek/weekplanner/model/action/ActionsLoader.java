package pl.mczernek.weekplanner.model.action;

import java.util.List;

public interface ActionsLoader {

    void loadActions(ActionLoaderListener listener);

    interface ActionLoaderListener{

        void onSuccess(List<Action> result);

        void onError(Throwable error);

    }

}
