package pl.mczernek.weekplanner;


import android.app.Application;

import com.facebook.stetho.Stetho;

public class WeekPlannerApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if(BuildConfig.DEBUG){
            Stetho.initializeWithDefaults(this);
        }
    }
}
